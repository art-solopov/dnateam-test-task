# frozen_string_literal: true

require 'minitest/autorun'
require_relative '../dnateam'

class LookAndSayGeneratorText < Minitest::Test
  # The first_25.txt file downloaded from the sequence's page
  # at OEIS: https://oeis.org/A005150
  # Author: T. D. Noe

  def setup
    @generator = LookAndSayGenerator.new
  end

  def test_first_25_members
    data_file_path = File.absolute_path File.join(__FILE__, '..', 'files', 'first_25.txt')
    data = File.open(data_file_path)
               .map { |line| line.match(/\d+ (\d+)/) }
               .compact
               .map { |md| md[1].to_i }
    assert_equal @generator.take(25), data
  end
end
