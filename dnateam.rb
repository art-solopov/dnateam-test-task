# frozen_string_literal: true

class LookAndSayGenerator
  # Класс для генерации последовательности Look-And-Say
  # A005150 в OEIS: https://oeis.org/A005150

  include Enumerable

  def initialize(start = 1)
    @start = Integer(start)
  end

  def each
    current = @start
    loop do
      yield current
      current = next_item current
    end
  end

  private

  def next_item(number)
    dec = number.to_s
    dec.each_char
       .slice_when { |a, b| a != b }
       .map { |numbers| "#{numbers.count}#{numbers.first}" }
       .join('')
       .then { |num| Integer(num) }
  end
end
