# Тестовое задание для DNA Team

Задача - генерация числовой последовательности https://oeis.org/A005150 (1, 11, 21, 1211, ...)

## Структура и пример использования

Для генерации последовательности используется класс `LookAndSayGenerator`.

```ruby
require './dnateam'

generator = LookAndSayGenerator.new
generator.take(7) # => [1, 11, 21, 1211, 111221, 312211, 13112221]
```

## Запуск тестов

Тесты запускаются с помощью `minitest/autorun`:

```bash
ruby test/*_test.rb
```